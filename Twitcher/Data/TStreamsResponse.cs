﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Twitcher.Data
{
    public class LinksStreamResponse
    {
        public string summary { get; set; }
        public string followed { get; set; }
        public string next { get; set; }
        public string featured { get; set; }
        public string self { get; set; }
    }

    public class TStreamsResponse
    {
        public int _total { get; set; }
        public List<TStream> streams = new List<TStream>();
        public LinksStreamResponse _links { get; set; }

        public bool IsEmpty => streams.Count == 0;

        public TStreamsResponse()
        { }

        public TStreamsResponse(string content)
        {
            var tResponse = JsonConvert.DeserializeObject<TStreamsResponse>(content);
            this._total = tResponse._total;
            this.streams = tResponse.streams;
        }
    }
}