﻿using System;
using Newtonsoft.Json;

namespace Twitcher.Data
{

    public class LinksGame
    {
    }

    public class TGame
    {
        public string name { get; set; }
        public int popularity { get; set; }
        public int _id { get; set; }
        public int giantbomb_id { get; set; }
        public TBox box { get; set; }
        public TLogo logo { get; set; }
        public LinksGame _links { get; set; }
    }
}