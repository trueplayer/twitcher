﻿namespace Twitcher.Data
{
    public class TPreview
    {
        public string small { get; set; }
        public string medium { get; set; }
        public string large { get; set; }
        public string template { get; set; }
    }
}