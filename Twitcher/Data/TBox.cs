﻿using Newtonsoft.Json;

namespace Twitcher.Data
{
    public class TBox
    {
        public string large { get; set; }
        public string medium { get; set; }
        public string small { get; set; }
        public string template { get; set; }
    }
}